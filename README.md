Mark Show
=========

A plugin to show the selections stored in Kakoune's default mark register.

After installing the plugin,
start Kakoune,
enable the plugin with:

    :mark-show-enable<ret>

...then select some text and save it with `Z`.

You can disable the plugin with:

    :mark-show-disable<ret>

To clear saved selections without disabling the plugin:

    :set-register ^<ret>

Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `mark-show.kak` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-mark-show.git

TODO
====

  - Support registers other than `"^`
      - Some register names are not legal characters
        in the names of options, hooks, faces, etc.
        Those registers have aliases (like `caret` for `^`)
        but those aliases can't be used for the `RegisterModified` hook,
        and automatically converting in both directions would be a pain.
